## Контрагенты  
- `GET` /contractors/ - Получаем всех контрагентов удовлетвроряющих фильтру в запросе
- `GET` /contractors/:id - Получаем контрагента  c указанным id из QBIS
- `POST` /contractors/add/ - Добавить контрагента   
- `POST` /contractors/update/:id - Обновить контрагента с указанным id